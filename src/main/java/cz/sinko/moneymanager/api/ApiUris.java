package cz.sinko.moneymanager.api;

public abstract class ApiUris {

	public static final String SLASH = "/";
	public static final String ROOT_URI_ACCOUNTS = "/accounts";

}
